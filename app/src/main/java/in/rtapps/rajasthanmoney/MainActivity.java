package in.rtapps.rajasthanmoney;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    public static final String MyPREFERENCES = "MyPrefs" ;
    private Button enableLogin,enableRegister,btnRegister,btnLogin;
    CardView registerView,loginView;
    //private String bamashadetailUrl="bahmashah/hofAndMembers/";
    private String bamashadetailUrl="family/details/";
    private String authUserapi="authUser.php";
    private EditText bmidlogin,bmidregister,passwordText,mobileNumberText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnLogin= (Button) findViewById(R.id.btnlogin);
        btnRegister= (Button) findViewById(R.id.btnregister);
        enableLogin = (Button) findViewById(R.id.loginenable);
        enableRegister = (Button) findViewById(R.id.registerenable);
        registerView = (CardView) findViewById(R.id.cvregister);
        loginView = (CardView) findViewById(R.id.cvlogin);
        bmidlogin= (EditText) findViewById(R.id.bmslogin);
        bmidregister= (EditText) findViewById(R.id.bmsreg);
        mobileNumberText= (EditText) findViewById(R.id.mobileno);
        passwordText= (EditText) findViewById(R.id.password);


        enableLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerView.setVisibility(View.GONE);
                loginView.setVisibility(View.VISIBLE);
            }
        });
        enableRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginView.setVisibility(View.GONE);
                registerView.setVisibility(View.VISIBLE);
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateRegister();
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateLogin();
            }
        });



    }
    public void validateLogin()
    {
        bmidlogin.setError(null);
        passwordText.setError(null);
        String bmId = bmidlogin.getText().toString();
        String password = passwordText.getText().toString();
        boolean cancel = false;
        View focusView = null;
        if (TextUtils.isEmpty(bmId)) {
            bmidlogin.setError(getString(R.string.error_field_required));
            focusView = bmidlogin;
            cancel = true;
        }
        else if(bmId.length()!=15)
        {
            bmidlogin.setError(getString(R.string.incorrect_bmid));
            focusView = bmidlogin;
            cancel = true;
        }

        if(TextUtils.isEmpty(password))
        {
            passwordText.setError(getString(R.string.error_field_required));
            focusView = passwordText;
            cancel = true;
        }
        else if(password.length()!=4)
        {
            passwordText.setError(getString(R.string.incorrect_pin_length));
            focusView = passwordText;
            cancel = true;
        }
        if(cancel)
        {
            focusView.requestFocus();
        }
        else
        {
            performLogin(bmId,password);
        }
    }
    public void validateRegister()
    {
        bmidregister.setError(null);
        mobileNumberText.setError(null);
        String bmId = bmidregister.getText().toString();
        String mobileNumber = mobileNumberText.getText().toString();
        boolean cancel = false;
        View focusView = null;
        if (TextUtils.isEmpty(bmId)) {
            bmidregister.setError(getString(R.string.error_field_required));
            focusView = bmidregister;
            cancel = true;
        }
        else if(bmId.length()!=15)
        {
            bmidregister.setError(getString(R.string.incorrect_bmid));
            focusView = bmidregister;
            cancel = true;
        }

        if(TextUtils.isEmpty(mobileNumber))
        {
            mobileNumberText.setError(getString(R.string.error_field_required));
            focusView = mobileNumberText;
            cancel = true;
        }
        else if(mobileNumber.length()!=12)
        {
            mobileNumberText.setError("Incorrect Aadhar Id");
            focusView = mobileNumberText;
            cancel = true;
        }
        if(cancel)
        {
            focusView.requestFocus();
        }
        else
        {
            checkDataWithBhamsa(bmId,mobileNumber);
        }
    }
    public void performLogin(final String bmid, final String password)
    {
        String urlGetBamashaDetail = getResources().getString(R.string.aws_ip)+authUserapi;
        StringRequest getbidDetails = new StringRequest(Request.Method.POST, urlGetBamashaDetail,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jresponse = new JSONObject(response);
                            if(jresponse.getInt("success") == 1)
                            {
                                int user_id=jresponse.getInt("user_id");
                                String data=jresponse.getString("data");
                                Long amt=jresponse.getLong("amt");
                                RajasthanMoneyApp.writetoPreferences(RajasthanMoneyApp.getAppContext(),"user_id",user_id);
                                RajasthanMoneyApp.writetoPreferences(RajasthanMoneyApp.getAppContext(),"data",data);
                                RajasthanMoneyApp.writetoPreferences(RajasthanMoneyApp.getAppContext(),"amt",amt);
                                Intent redirecthome=new Intent(MainActivity.this,Home.class);
                                startActivity(redirecthome);
                                finish();
                            }
                            else
                            {
                                Toast.makeText(MainActivity.this,"Incorrect Pin Number !!!",Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            Toast.makeText(MainActivity.this,"Something Went Wrong !!!",Toast.LENGTH_SHORT).show();

                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                performLogin(bmid,password);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("bmid",bmid);
                params.put("pin",password);
                return params;
            }
        };
        RajasthanMoneyApp.getInstance().addToRequestQueue(getbidDetails,"CREATEREGITER");

    }

    public void checkDataWithBhamsa(final String bmId, final String mobileNo)
    {
        String urlGetBamashaDetail = getResources().getString(R.string.main_api_url)+bamashadetailUrl+bmId+"?client_id="+getResources().getString(R.string.clid);
        StringRequest getbidDetails = new StringRequest(Request.Method.GET, urlGetBamashaDetail,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject apiresult=new JSONObject(response);
                            JSONObject hof_Details=apiresult.getJSONObject("hof_Details");
                            if(hof_Details.length()==0)
                            {
                                Toast.makeText(MainActivity.this,"Incorrect Bhamasha Id !!!",Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                String uID=hof_Details.getString("AADHAR_ID");
                                String name=hof_Details.getString("NAME_HND");
                                if(uID.equalsIgnoreCase(mobileNo))
                                {
                                    //Verified Create Wallet
                                    Toast.makeText(MainActivity.this,"Welcome "+name+" !!!",Toast.LENGTH_SHORT).show();
                                    Intent register=new Intent(MainActivity.this,Registration.class);
                                    register.putExtra("data",hof_Details.toString());
                                    startActivity(register);
                                }
                                else
                                {
                                    Toast.makeText(MainActivity.this,"Incorrect Aadhar Number !!!",Toast.LENGTH_SHORT).show();
                                    //UnVerified Show Toast
                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(MainActivity.this,"Something Went Wrong !!!",Toast.LENGTH_SHORT).show();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                checkDataWithBhamsa(bmId,mobileNo);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("client_id", getResources().getString(R.string.clid));
                return params;
            }
        };
        RajasthanMoneyApp.getInstance().addToRequestQueue(getbidDetails,"BHAMASADETAILS");
    }

    private void createnewUser(final String pin) {


    }


}
