package in.rtapps.rajasthanmoney;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import in.rtapps.rajasthanmoney.adapter.Hod_adapter;
import in.rtapps.rajasthanmoney.CircleImageView;
import in.rtapps.rajasthanmoney.model.HeadofDetails;


/**
 * A simple {@link Fragment} subclass.
 */
public class BhamashahCard extends Fragment {


    private TextView bhm_id,aadhar_id,headname;
    private RecyclerView recyclerView;
    private Hod_adapter adapter;
    private ArrayList<HeadofDetails> hod_list = new ArrayList<HeadofDetails>();
    private String bamashadetailUrl = "hofAndMember/ForApp/";
    public static  String bmid="";
    private static  String fmid="";
    public  static String acc_no;
    public   static  String ifsc;
    private  String name="";
    public static in.rtapps.rajasthanmoney.CircleImageView head;


    public BhamashahCard() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_bhamashah_card, container, false);
        String bmsdata=RajasthanMoneyApp.readFromPreferences(RajasthanMoneyApp.getAppContext(),"data","");
        if(bmsdata.length()>0)
        {
            try {
                JSONObject ja= new JSONObject(bmsdata);
                fmid=ja.getString("FAMILYIDNO");
                bmid=ja.getString("BHAMASHAH_ID");
            } catch (JSONException e)
            {
                e.printStackTrace();
            }
        }


        headname = (TextView) v.findViewById(R.id.headname);
        aadhar_id = (TextView) v.findViewById(R.id.aadhar_id);
        bhm_id = (TextView) v.findViewById(R.id.bhm_id);
        head= (CircleImageView) v.findViewById(R.id.head_image);
        Drawable dr=((CircleImageView)getActivity().findViewById(R.id.imgProfilePic1)).getDrawable();
               head.setImageDrawable(dr);

        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        adapter = new Hod_adapter(getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
        FetchData();
        return  v;

    }
    private void FetchData() {
        {
            String urlGetBamashaDetail = getResources().getString(R.string.main_api_url) +bamashadetailUrl+ fmid+ "?client_id=" + getResources().getString(R.string.clid);
            StringRequest getbidDetails = new StringRequest(Request.Method.GET, urlGetBamashaDetail,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                hod_list = new ArrayList<>();
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject hoddata = jsonObject.getJSONObject("hof_Details");
                                String aadharid = hoddata.getString("AADHAR_ID");
                                String name = hoddata.getString("NAME_ENG");
                                bmid = hoddata.getString("BHAMASHAH_ID");
                                ifsc=hoddata.getString("IFSC_CODE");
                                acc_no=hoddata.getString("ACC_NO");
                                bhm_id.setText(bmid);
                                headname.setText(name);
                                aadhar_id.setText(aadharid);
                                JSONArray familydata = jsonObject.getJSONArray("family_Details");
                                for (int i = 0; i < familydata.length(); i++)
                                {
                                    JSONObject json_data = familydata.getJSONObject(i);
                                    HeadofDetails familyDetails = new HeadofDetails();
                                    familyDetails.NAME_ENG = json_data.getString("NAME_ENG");
                                    familyDetails.GENDER = json_data.getString("GENDER");
                                    familyDetails.RELATION_ENG = json_data.getString("RELATION_ENG");
                                    familyDetails.MOTHER_NAME_ENG = json_data.getString("MOTHER_NAME_ENG");
                                    familyDetails.M_ID = json_data.getString("M_ID");
                                    hod_list.add(familyDetails);
                                }
                                adapter.setHodlist(hod_list);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("client_id", getResources().getString(R.string.clid));
                    return params;
                }
            };
            RajasthanMoneyApp.getInstance().addToRequestQueue(getbidDetails, "BHAMASADETAILS");

        }

    }


}
