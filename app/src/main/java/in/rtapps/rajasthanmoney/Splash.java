package in.rtapps.rajasthanmoney;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Splash extends AppCompatActivity {

    private Handler handler =  new Handler();
    private Runnable splashover;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        splashover =  new Runnable() {
            @Override
            public void run() {
                //appname.clearAnimation();
                if(RajasthanMoneyApp.readFromPreferences(RajasthanMoneyApp.getAppContext(),"user_id",0) > 0)
                {
                    //Already Registered Redirect to Drawer
                    Intent in = new Intent(Splash.this,Home.class);
                    startActivity(in);
                    finish();
                }
                else
                {
                    Intent in = new Intent(Splash.this,MainActivity.class);
                    startActivity(in);
                    finish();
                }

            }
        };

        handler.postDelayed(splashover, 30 * 30);
    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        handler.removeCallbacks(splashover);
    }
}
