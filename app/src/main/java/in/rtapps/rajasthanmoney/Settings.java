package in.rtapps.rajasthanmoney;


import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

import static in.rtapps.rajasthanmoney.R.id.autocash;


/**
 * A simple {@link Fragment} subclass.
 */
public class Settings extends Fragment {

    public static final String MyPREFERENCES = "MyPrefs";
    public static final String photoUrl = "photoUrl";
    public static final String acc_no = "acc_no";
    public static final String ifsc_code = "ifsc_code";
    public static final String switchstatus ="switchstate";

    private boolean mUserLearnedDrawer;
    private boolean mFromSavedBundle;

    TextView setacnoTV,setifscTV,setbmTV;
    SwitchCompat autocash;

    SharedPreferences sharedpreferences;
    private static final String TAG_SUCCESS = "success";

    int success;
    CircleImageView prof;

    public Settings() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_settings, container, false);
        setbmTV = (TextView) fragmentView.findViewById(R.id.set_bm_id);

        setacnoTV = (TextView) fragmentView.findViewById(R.id.set_acno_TV);
        setifscTV = (TextView) fragmentView.findViewById(R.id.set_ifsc_TV);
        prof= (CircleImageView) fragmentView.findViewById(R.id.setting_profpic);

        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/roboto_thin.ttf");
        setacnoTV.setTypeface(tf);
        setifscTV.setTypeface(tf);
        setbmTV.setTypeface(tf);
        setbmTV.setText(BhamashahCard.bmid);
        setacnoTV.setText(BhamashahCard.acc_no);
        setifscTV.setText(BhamashahCard.ifsc);

        autocash = (SwitchCompat) fragmentView.findViewById(R.id.autocash);
        autocash.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    //api call needed -> server to enable auto cash
                    //call  SetAutoCash

                    Toast.makeText(getActivity(),"Auto Cash Out Enabled",Toast.LENGTH_SHORT).show();

                }else{
                    //api call needed -> call server to disable auto cash
                    //call SetAutoCash()
                    Toast.makeText(getActivity(),"Auto Cash Out Disabled",Toast.LENGTH_SHORT).show();

                }

            }

        });

        //
        return fragmentView;
    }




}
