package in.rtapps.rajasthanmoney;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import in.rtapps.rajasthanmoney.db.ChangeDB;
import in.rtapps.rajasthanmoney.db.DatabaseHelper;


public class DatabaseManager {
    private DatabaseHelper mHelper;
    private SQLiteDatabase mDatabase;

    public DatabaseManager(Context context) {
        mHelper = new DatabaseHelper(context, ChangeDB.DB_NAME, null, ChangeDB.DB_VERSION);
        mDatabase = mHelper.getWritableDatabase();
    }

}
