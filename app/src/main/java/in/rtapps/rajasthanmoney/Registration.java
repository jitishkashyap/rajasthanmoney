package in.rtapps.rajasthanmoney;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Registration extends AppCompatActivity {
    private TextView nametxt,fidtxt,bmidtxt,uidtxt;
    private EditText pin1,pin2;
    private Button signup;
    String bmsdata;
    String nmtxt,bmtxt,fitxt,uitxt,pintxt;
    String createUserapi="createUser.php";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        if(getIntent().hasExtra("data"))
        {
            bmsdata=getIntent().getStringExtra("data");
            nametxt= (TextView) findViewById(R.id.nametxt);
            fidtxt= (TextView) findViewById(R.id.fidtxt);
            bmidtxt= (TextView) findViewById(R.id.bmidtxt);
            uidtxt= (TextView) findViewById(R.id.uidtxt);
            pin1= (EditText) findViewById(R.id.pin1);
            pin2= (EditText) findViewById(R.id.pin2);
            signup= (Button) findViewById(R.id.btnregister);
            try {
                JSONObject bms=new JSONObject(bmsdata);
                 nmtxt=bms.getString("NAME_ENG");
                 bmtxt=bms.getString("BHAMASHAH_ID");
                 uitxt=bms.getString("AADHAR_ID");
                 fitxt=bms.getString("FAMILYIDNO");
                nametxt.setText(nmtxt);
                fidtxt.setText(fitxt);
                bmidtxt.setText(bmtxt);
                uidtxt.setText(uitxt);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            signup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    validatePin();
                }
            });


        }
        else
        {
            super.onBackPressed();
        }



    }

    private void validatePin() {

        pin2.setError(null);
        pin1.setError(null);
        String pin1txt = pin1.getText().toString();
        String pin2txt = pin2.getText().toString();
        boolean cancel = false;
        View focusView = null;
        if (TextUtils.isEmpty(pin1txt)) {
            pin1.setError(getString(R.string.error_field_required));
            focusView = pin1;
            cancel = true;
        }
        else if(pin1txt.length()!=4)
        {
            pin1.setError("4 Digits Required");
            focusView = pin1;
            cancel = true;
        }
        if (TextUtils.isEmpty(pin2txt)) {
            pin2.setError(getString(R.string.error_field_required));
            focusView = pin1;
            cancel = true;
        }
        else if(pin1txt.length()!=4)
        {
            pin2.setError("4 Digits Required");
            focusView = pin2;
            cancel = true;
        }
        else if(!pin1txt.equalsIgnoreCase(pin2txt))
        {
            pin2.setError("Both Pin should be Same");
            focusView = pin2;
            cancel = true;
        }

        if(cancel)
        {
            focusView.requestFocus();
        }
        else
        {
            createnewUser(pin1txt);
        }

    }

    private void createnewUser(final String pin) {

        String urlGetBamashaDetail = getResources().getString(R.string.aws_ip)+createUserapi;
        StringRequest getbidDetails = new StringRequest(Request.Method.POST, urlGetBamashaDetail,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jresponse = new JSONObject(response);
                            if(jresponse.getInt("success") == 1)
                            {
                                int user_id=jresponse.getInt("user_id");
                                RajasthanMoneyApp.writetoPreferences(RajasthanMoneyApp.getAppContext(),"user_id",user_id);
                                RajasthanMoneyApp.writetoPreferences(RajasthanMoneyApp.getAppContext(),"data",bmsdata);
                                RajasthanMoneyApp.writetoPreferences(RajasthanMoneyApp.getAppContext(),"amt",0);
                                Intent redirecthome=new Intent(Registration.this,Home.class);
                                startActivity(redirecthome);
                                finish();
                            }
                            else
                            {
                                Toast.makeText(Registration.this,"User Creation Failed !!!",Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            Toast.makeText(Registration.this,"Something Went Wrong !!!",Toast.LENGTH_SHORT).show();

                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                createnewUser(pin);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name",nmtxt);
                params.put("bmid",bmtxt);
                params.put("uid",uitxt);
                params.put("fmid",fitxt);
                params.put("data",bmsdata);
                params.put("pin",pin);
                return params;
            }
        };
        RajasthanMoneyApp.getInstance().addToRequestQueue(getbidDetails,"CREATEREGITER");
    }
}
