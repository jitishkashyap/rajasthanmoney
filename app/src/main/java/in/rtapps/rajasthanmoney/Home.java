package in.rtapps.rajasthanmoney;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Home extends AppCompatActivity {


    Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    ActionBarDrawerToggle mdrawertoggle;
    DrawerLayout mdrawerlayout;
    SharedPreferences sharedpreferences;
    private ListView mDrawerList, mLogoutList;
    private String name,fmid,bmid;
    private String bamashaphotoUrl = "hofMembphoto/";


    String[] Title ={"Home","Bhamashah Card","Bhamashah Wallet","Transfer Money","","Settings","Help","Share App","Logout"};
    private boolean isHome=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        sharedpreferences = getSharedPreferences(MainActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        mToolbar = (Toolbar) findViewById(R.id.tbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView)findViewById(R.id.list);
        addDrawerItems();
        setUp(mDrawerLayout, mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        CircleImageView imgProfilePic = (CircleImageView) findViewById(R.id.imgProfilePic1);
        TextView txtName1 = (TextView) findViewById(R.id.txtName1);
        String bmsdata=RajasthanMoneyApp.readFromPreferences(RajasthanMoneyApp.getAppContext(),"data","");
        if(bmsdata.length()>0)
        {
            try {
                JSONObject ja= new JSONObject(bmsdata);
                fmid=ja.getString("FAMILYIDNO");
                bmid=ja.getString("BHAMASHAH_ID");
                name=ja.getString("NAME_ENG");
                String name1=ja.getString("NAME_HND");
                fetchimage("0",imgProfilePic);
                txtName1.setText(name+" ("+name1+")");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }
    public void setUp(DrawerLayout dLayout, final Toolbar tbar) {


        mdrawerlayout=dLayout;
        mdrawertoggle=new ActionBarDrawerToggle(this,mdrawerlayout,tbar,R.string.navigation_drawer_open,R.string.navigation_drawer_close){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                if(slideOffset<.6)
                {

                }
            }

        };
        mdrawerlayout.setDrawerListener(mdrawertoggle);


        mdrawerlayout.post(new Runnable() {
            @Override
            public void run() {
                mdrawertoggle.syncState();
            }
        });



        displayView(0);

    }
    public void signOut(View v) {
        SharedPreferences sharedpreferences = getSharedPreferences
                (MainActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.apply();
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }
    private void displayView(int position) {
        // update the main content by replacing fragments
        Fragment fragment = null;


        switch (position) {


            case 0:
             fragment = new HomeFragement();
                isHome=true;

                break;
            case 1:
               fragment = new BhamashahCard();
                isHome=false;

                break;

            case 2:
                fragment = new Wallet();
                isHome=false;

                break;

            case 3:
              fragment = new Transfers();
                isHome=false;


                break;
            case 5:
              fragment = new Settings();
                isHome=false;



                break;
            case 6:
//                fragment = new Help();
                isHome=false;



                break;
            case 7:
//                fragment = new Share();
                isHome=false;

            case 8:

                break;

            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, fragment).addToBackStack("fragBack").commit();

            mDrawerLayout.closeDrawers();
            // update selected item and title, then close the drawer
            if(position != 8)
            {
                setTitle(Title[position]);
            }


        } else {
            // error in creating fragment
            mDrawerLayout.closeDrawers();

        }

    }
    public void setTitle(CharSequence title) {
        CharSequence mTitle = title;
        getSupportActionBar().setTitle(mTitle);

    }
    private void addDrawerItems() {

        String[] Title ={"Home","BhamshahCard","Wallet","Transactions","Settings","Help","Share App","Logout"};

        NavigationDrawerItem[] data = new NavigationDrawerItem[8];
        data[0] = new NavigationDrawerItem(R.mipmap.ic_launcher,"Home");
        data[1] = new NavigationDrawerItem(R.mipmap.ic_launcher,"Bhamashah Card");
        data[2] = new NavigationDrawerItem(R.mipmap.ic_launcher,"Bhamashah Wallet");
        data[3] = new NavigationDrawerItem(R.mipmap.ic_launcher,"Transfer Money");
        data[4] = new NavigationDrawerItem(0,"");
        data[5] = new NavigationDrawerItem(R.mipmap.ic_launcher,"Settings");
        data[6] = new NavigationDrawerItem(R.mipmap.ic_launcher,"Help");
        data[7] = new NavigationDrawerItem(R.mipmap.ic_launcher,"Share App");


        mDrawerList.setAdapter(new NavigationAdapter(this, R.layout.navigation_list_item, data));

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


              if (position == 0) {

                    displayView(position);

                } else if (position == 1) {
                    displayView(position);

                } else if (position == 2) {
                    displayView(position);


                } else if (position == 3) {
                    displayView(position);


                } else if (position == 5) {

                    displayView(position);

                } else if (position == 6) {

                    displayView(position);

                } else if (position == 7) {

                  displayView(position);
              }
                else if(position==8)
              {
              }

            }
        });


    }
    @Override
    protected void onResume() {


        super.onResume();
    }

    protected void onStart() {
        super.onStart();
    }
    @Override
    public void onBackPressed() {



    }
    private void fetchimage(String m_id, final ImageView imageview) {
        String urlGetBamashaDetail = getResources().getString(R.string.main_api_url) + bamashaphotoUrl + bmid+"/" + m_id + "?client_id=" + getResources().getString(R.string.clid);
        StringRequest getbidDetails = new StringRequest(Request.Method.GET, urlGetBamashaDetail,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject jsonArray = jsonObject.getJSONObject("hof_Photo");
                            String photo = jsonArray.getString("PHOTO");
                            byte[] decodedString = Base64.decode(photo, Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            imageview.setImageBitmap(decodedByte);
                            if(BhamashahCard.head.hasWindowFocus())
                            {
                                BhamashahCard.head.setImageBitmap(decodedByte);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("client_id", getResources().getString(R.string.clid));
                return params;
            }
        };
        RajasthanMoneyApp.getInstance().addToRequestQueue(getbidDetails, "BHAMASADETAILS");
    }
}
