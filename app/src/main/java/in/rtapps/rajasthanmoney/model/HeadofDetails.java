package in.rtapps.rajasthanmoney.model;

/**
 * Created by jitish on 20-03-2017.
 */

public class HeadofDetails {

    public  String BHAMASHAH_ID, CATEGORY_DESC_ENG, STATE, RELATION_ENG, NAME_ENG, GENDER, FATHER_NAME_ENG, MOTHER_NAME_ENG, M_ID, VILLAGE_NAME;

    public void setBHAMASHAH_ID(String BHAMASHAH_ID) {
        this.BHAMASHAH_ID = BHAMASHAH_ID;
    }

    public String getCATEGORY_DESC_ENG() {
        return CATEGORY_DESC_ENG;
    }

    public void setCATEGORY_DESC_ENG(String CATEGORY_DESC_ENG) {
        this.CATEGORY_DESC_ENG = CATEGORY_DESC_ENG;
    }

    public String getSTATE() {
        return STATE;
    }

    public void setSTATE(String STATE) {
        this.STATE = STATE;
    }

    public String getRELATION_ENG() {
        return RELATION_ENG;
    }

    public void setRELATION_ENG(String RELATION_ENG) {
        this.RELATION_ENG = RELATION_ENG;
    }

    public String getNAME_ENG() {
        return NAME_ENG;
    }

    public void setNAME_ENG(String NAME_ENG) {
        this.NAME_ENG = NAME_ENG;
    }

    public String getGENDER() {
        return GENDER;
    }

    public void setGENDER(String GENDER) {
        this.GENDER = GENDER;
    }

    public String getFATHER_NAME_ENG() {
        return FATHER_NAME_ENG;
    }

    public void setFATHER_NAME_ENG(String FATHER_NAME_ENG) {
        this.FATHER_NAME_ENG = FATHER_NAME_ENG;
    }

    public String getMOTHER_NAME_ENG() {
        return MOTHER_NAME_ENG;
    }

    public void setMOTHER_NAME_ENG(String MOTHER_NAME_ENG) {
        this.MOTHER_NAME_ENG = MOTHER_NAME_ENG;
    }

    public String getM_ID() {
        return M_ID;
    }

    public void setM_ID(String m_ID) {
        M_ID = m_ID;
    }

    public String getVILLAGE_NAME() {
        return VILLAGE_NAME;
    }

    public void setVILLAGE_NAME(String VILLAGE_NAME) {
        this.VILLAGE_NAME = VILLAGE_NAME;
    }


}
