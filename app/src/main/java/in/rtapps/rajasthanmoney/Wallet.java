package in.rtapps.rajasthanmoney;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.text.DecimalFormat;



/**
 * A simple {@link Fragment} subclass.
 */
public class Wallet extends Fragment {


    public Wallet() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_wallet, container, false);
        TextView mRupee, mAmount, mAmountDecimal;
        mRupee = (TextView) v.findViewById(R.id.setting_rupeeTV);
        mAmount = (TextView) v.findViewById(R.id.setting_amount);
        mAmountDecimal = (TextView) v.findViewById(R.id.setting_amount_decimal);
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/indian_rupee.ttf");
        mRupee.setTypeface(tf);
        tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/roboto_thin.ttf");
        mAmount.setTypeface(tf);
        Double amountInDecimal = 7889.89;
        String amt = (new DecimalFormat("##,##,###.00").format(amountInDecimal)).toString();
        mAmount.setText("" + amt.substring(0, amt.length() - 2));
        mAmountDecimal.setText("" + amt.substring(amt.length() - 2));

        return v;
    }

}
