package in.rtapps.rajasthanmoney.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.rtapps.rajasthanmoney.BhamashahCard;
import in.rtapps.rajasthanmoney.R;
import in.rtapps.rajasthanmoney.RajasthanMoneyApp;
import in.rtapps.rajasthanmoney.model.HeadofDetails;

/**
 * Created by Ravi Tamada on 18/05/16.
 */
public class Hod_adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<HeadofDetails> hod_List = new ArrayList<HeadofDetails>();
    private LayoutInflater inflater;
    private String bamashaphotoUrl = "hofMembphoto/";

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView headname, relationship, aadhar_id, sextype;
        public ImageView imageview;

        public MyViewHolder(View view) {
            super(view);
            headname = (TextView) view.findViewById(R.id.headname);
            relationship = (TextView) view.findViewById(R.id.relationship);
            aadhar_id = (TextView) view.findViewById(R.id.aadhar_id);
            sextype = (TextView) view.findViewById(R.id.sextype);
            imageview = (ImageView) view.findViewById(R.id.head_image);
        }
    }


    public Hod_adapter(Context mContext) {
        this.mContext = mContext;
        inflater = LayoutInflater.from(mContext);
    }

    public void setHodlist(ArrayList<HeadofDetails> hd) {
        this.hod_List = hd;
        notifyDataSetChanged();
    }

    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.hod_list_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MyViewHolder myHolder = (MyViewHolder) holder;
        HeadofDetails family_details = hod_List.get(position);
        myHolder.headname.setText(family_details.getNAME_ENG());
        myHolder.relationship.setText(family_details.getRELATION_ENG());
        myHolder.sextype.setText(family_details.getGENDER());
        fetchimage(family_details.getM_ID(), myHolder.imageview);

    }

    private void fetchimage(String m_id, final ImageView imageview) {
        String urlGetBamashaDetail = mContext.getResources().getString(R.string.main_api_url) + bamashaphotoUrl + BhamashahCard.bmid +"/" + m_id + "?client_id=" + mContext.getResources().getString(R.string.clid);
        StringRequest getbidDetails = new StringRequest(Request.Method.GET, urlGetBamashaDetail,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("member_Photo");
                            JSONObject imageString = jsonArray.getJSONObject(0);
                            String photo = imageString.getString("PHOTO");
                            byte[] decodedString = Base64.decode(photo, Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            imageview.setImageBitmap(decodedByte);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("client_id", mContext.getResources().getString(R.string.clid));
                return params;
            }
        };
        RajasthanMoneyApp.getInstance().addToRequestQueue(getbidDetails, "BHAMASADETAILS");
    }


    @Override
    public int getItemCount() {
        return hod_List.size();
    }
}