package in.rtapps.rajasthanmoney;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by grk4675 on 20-07-2016.
 */
public class NavigationAdapter extends ArrayAdapter<NavigationDrawerItem> {
    private final Context context;
    private final int layoutResourceId;
    private NavigationDrawerItem data[] = null;

    public NavigationAdapter(Context context, int layoutResourceId, NavigationDrawerItem data[]) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = ((AppCompatActivity) context).getLayoutInflater();


        View v = inflater.inflate(layoutResourceId, parent, false);
        if(position==4)
        {
            ViewGroup avs = (ViewGroup) v.findViewById(R.id.layoutSeparator);
            ViewGroup avs1 = (ViewGroup) v.findViewById(R.id.layoutDados);

            avs.setVisibility(View.VISIBLE);
            avs1.setVisibility(View.GONE);

        }
        else {

            TextView textView = (TextView) v.findViewById(R.id.navDrawerTextView);
            ImageView imview = (ImageView) v.findViewById(R.id.navDrawerImageView);
            NavigationDrawerItem choice = data[position];
            //imageView.setImageDrawable();
            textView.setText(choice.name);
            imview.setImageResource(choice.icon);
        }

        return v;

    }
}
class NavigationDrawerItem
{
    public int icon;
    public String name;

    public NavigationDrawerItem(int icon, String name)
    {
        this.icon = icon;
        this.name = name;
    }
}