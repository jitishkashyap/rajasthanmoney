package in.rtapps.rajasthanmoney.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by grk4675 on 21-07-2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, ChangeDB.DB_NAME, null, ChangeDB.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqlDB) {
        String sqlQuery =
                String.format("CREATE TABLE %s (" +
                                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                "%s STRING ," +
                                "%s STRING," +
                                "%s STRING," +
                                "%s STRING," +
                                "%s STRING," +
                                "%s INTEGER)",
                        ChangeDB.TABLE_PETITION,
                        ChangeDB.PETITION.PetID,
                        ChangeDB.PETITION.TITLE,
                        ChangeDB.PETITION.AUTHOR,
                        ChangeDB.PETITION.CATEGORY,
                        ChangeDB.PETITION.PHOTOURL,
                        ChangeDB.PETITION.DOC
                        );

        Log.d("TaskDBHelper", "Query to form table: " + sqlQuery);
        sqlDB.execSQL(sqlQuery);
        sqlQuery =
                String.format("CREATE TABLE %s (" +
                                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                "%s STRING ," +
                                "%s STRING," +
                                "%s STRING," +
                                "%s STRING," +
                                "%s INTEGER,"+
                                "%s INTEGER)",
                        ChangeDB.TABLE_POLL,
                        ChangeDB.POLL.PollID,
                        ChangeDB.POLL.TITLE,
                        ChangeDB.POLL.AUTHOR,
                        ChangeDB.POLL.CATEGORY,
                        ChangeDB.POLL.DOCR,
                        ChangeDB.POLL.DOCS
                );

        Log.d("TaskDBHelper", "Query to form table: " + sqlQuery);
        sqlDB.execSQL(sqlQuery);
        sqlQuery =
                String.format("CREATE TABLE %s (" +
                                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                "%s STRING," +
                                "%s INTEGER)",
                        ChangeDB.TABLE_PET_SIGN,
                        ChangeDB.PET_SIGN.PetID,
                        ChangeDB.PET_SIGN.DOS
                );

        Log.d("TaskDBHelper", "Query to form table: " + sqlQuery);
        sqlDB.execSQL(sqlQuery);
        sqlQuery =
                String.format("CREATE TABLE %s (" +
                                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                "%s STRING," +
                                "%s STRING," +
                                "%s STRING," +
                                "%s STRING," +
                                "%s STRING," +
                                "%s STRING," +
                                "%s INTEGER)",
                        ChangeDB.TABLE_MY_PET,
                        ChangeDB.MY_PET.PetID,
                        ChangeDB.MY_PET.TITLE,
                        ChangeDB.MY_PET.DESC,
                        ChangeDB.MY_PET.DMAK,
                        ChangeDB.MY_PET.CATEGORY,
                        ChangeDB.MY_PET.PHOTOURL,
                        ChangeDB.MY_PET.DOC
                );

        Log.d("TaskDBHelper", "Query to form table: " + sqlQuery);
        sqlDB.execSQL(sqlQuery);
        sqlQuery =
                String.format("CREATE TABLE %s (" +
                                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                "%s STRING," +
                                "%s STRING," +
                                "%s STRING," +
                                "%s INTEGER)",
                        ChangeDB.TABLE_MY_POLL,
                        ChangeDB.MY_POLL.PollID,
                        ChangeDB.MY_POLL.TITLE,
                        ChangeDB.PETITION.CATEGORY,
                        ChangeDB.PETITION.DOC
                );

        Log.d("TaskDBHelper", "Query to form table: " + sqlQuery);
        sqlDB.execSQL(sqlQuery);
        sqlQuery =
                String.format("CREATE TABLE %s (" +
                                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                "%s STRING," +
                                "%s STRING)",
                        ChangeDB.TABLE_POLL_ENTRY,
                        ChangeDB.POLL_ENTRY.PollID,
                        ChangeDB.POLL_ENTRY.OPTION
                );

        Log.d("TaskDBHelper", "Query to form table: " + sqlQuery);
        sqlDB.execSQL(sqlQuery);
        sqlQuery =
                String.format("CREATE TABLE %s (" +
                                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                "%s INTEGER)",
                        ChangeDB.TABLE_DB_SYNC,
                        ChangeDB.DB_SYNC.TIMESTAMP
                );

        Log.d("TaskDBHelper", "Query to form table: " + sqlQuery);
        sqlDB.execSQL(sqlQuery);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqlDB, int i, int i1) {
        sqlDB.execSQL("DROP TABLE IF EXISTS "+ChangeDB.TABLE_PETITION);
        sqlDB.execSQL("DROP TABLE IF EXISTS "+ChangeDB.TABLE_MY_PET);
        sqlDB.execSQL("DROP TABLE IF EXISTS "+ChangeDB.TABLE_PET_SIGN);
        sqlDB.execSQL("DROP TABLE IF EXISTS "+ChangeDB.TABLE_MY_POLL);
        sqlDB.execSQL("DROP TABLE IF EXISTS "+ChangeDB.TABLE_POLL);
        sqlDB.execSQL("DROP TABLE IF EXISTS "+ChangeDB.TABLE_POLL_ENTRY);
        sqlDB.execSQL("DROP TABLE IF EXISTS "+ChangeDB.TABLE_DB_SYNC);

        onCreate(sqlDB);
    }
}
