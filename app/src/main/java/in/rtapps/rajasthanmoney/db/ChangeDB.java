package in.rtapps.rajasthanmoney.db;

import android.provider.BaseColumns;


public class ChangeDB {

    public static final String DB_NAME = "in.rtapps.change.db";
    public static final int DB_VERSION = 4;
    public static final String TABLE_PETITION = "Petition";
    public static final String TABLE_POLL = "Poll";
    public static final String TABLE_PET_SIGN="Pet_sign";
    public static final String TABLE_MY_PET="Pet_my";
    public static final String TABLE_MY_POLL="Poll_my";
    public static final String TABLE_DB_SYNC="db_sync";
    public static final String TABLE_POLL_ENTRY="Poll_entry";

    public class PETITION {
        public static final String _ID = BaseColumns._ID;
        public  static final String PetID ="petID";
        public static final String TITLE = "petTitle";
        public static final String AUTHOR = "petAuthor";
        public static final String CATEGORY= "petCategory";
        public static final String PHOTOURL = "petUrl";
        public static final String DOC = "petDoc";

    }
    public class POLL {
        public static final String _ID = BaseColumns._ID;
        public  static final String PollID ="pollID";
        public static final String TITLE = "pollTitle";
        public static final String AUTHOR = "pollAuthor";
        public static final String CATEGORY= "pollCategory";
        public static final String DOCR = "pollDOCR";
        public static final String DOCS ="pollDOCS";
    }
    public class PET_SIGN {
        public static final String _ID = BaseColumns._ID;
        public  static final String PetID ="petID";
        public static final String DOS = "petDos";
    }
    public class MY_PET {
        public static final String _ID = BaseColumns._ID;
        public  static final String PetID ="petID";
        public static final String TITLE = "petTitle";
        public static final String DESC="petDesc";
        public static final String DMAK="petDmak";
        public static final String CATEGORY= "petCategory";
        public static final String PHOTOURL = "petUrl";
        public static final String DOC = "petDoc";
    }
    public class MY_POLL {
        public static final String _ID = BaseColumns._ID;
        public  static final String PollID ="pollID";
        public static final String TITLE = "pollTitle";
        public static final String CATEGORY= "pollCategory";
        public static final String DOC = "pollDoc";
    }
    public class POLL_ENTRY {
        public static final String _ID = BaseColumns._ID;
        public static final String PollID = "pollID";
        public static final String OPTION= "pollOpt";
    }
    public class DB_SYNC {
        public static final String _ID = BaseColumns._ID;
        public static final String TIMESTAMP = "TIMESTAMP";

    }




}
